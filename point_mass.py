import numpy as np

from typing import Iterable

from helpers import *


class PointMass:
    def __init__(self, pos: Iterable[float] = (0.0, 0.0, 0.0), vel: [Iterable[float]] = (0.0, 0.0, 0.0), mass=1.0,
                 charge=0.0, fixed=False, negligible_gravity=False, negligible_charge=False, color='', size=8.0):
        """ A simulated point mass with specified physical properties (in standard SI units)

        Note: Setting charge or mass to zero or setting one of the `negligible_...` flags may simplify simulation

        :param pos: the initial position of the particle, (x, y, z) meters
        :param vel: the initial velocity of the particle, (x, y, z) m/s
        :param mass: the mass of the particle, in kg
        :param charge: the charge of the particle, in Coulombs
        :param fixed: if True, particle will remain fixed in place
        :param negligible_gravity: if True, this particle's gravitational effect on other objects will be ignored
        :param negligible_charge: if True, this particle's electrostatic effect on other objects will be ignored
        :param color: the particle's color, e.g. 'r' or 'red', leave blank for an automatic choice
        :param size: the size of the particle displayed on screen, in pixels
        """
        # User-Specified parameters

        self.pos = pos
        self.vel = vel

        self.mass = mass
        self.charge = charge

        self.fixed = fixed
        self.negligible_gravity = negligible_gravity
        self.negligible_charge = negligible_charge

        self.color = color
        self.size = size

        # Other/Internal variables

        # The current force on the particle
        self.force = np.array((0.0, 0.0, 0.0))

    def __repr__(self):
        return f'Point Mass [' \
               f'({str_vector(self.pos)}) m ' \
               f'({str_vector(self.vel)}) m/s ' \
               f'{self.mass: .1e} kg ' \
               f'{self.charge: .1e} C]'

    def __hash__(self):
        return hash((self.mass, self.charge, self.fixed, self.color, self.size))

    def __eq__(self, other):
        return hash(self) == hash(other)
