"""
    Convenient human-readable string formats for common situations
"""
from typing import Iterable
from datetime import datetime


def str_vector(vec: Iterable, fmt='.1e', sep=', '):
    """ Human-readable string representation of a vector

    With default settings, produces a result like "1.5e1, 0.0e0, 2.0e-2"

    :param vec: the vector/list to convert to a string
    :param fmt: the string formatter to use for each item in the vector (as accepted in an f-string)
    :param sep: the separator string to place between each item
    :return: a string representation of the given vector
    """
    return sep.join(f'{x:fmt}' for x in vec)


def str_timestamp(incl_date=True, incl_time=True, use_utc=False):
    """ Readable date/time stamp

    Not safe for inclusion in file names. See `str_file_safe_timestamp`

    :param incl_date: whether to include the date portion of the timestamp, defaults to True
    :param incl_time: whether to include the time portion of the timestamp, defaults to True
    :param use_utc: whether to use UTC time (instead of system timezone), defaults to False
    :return: a timestamp string that can be safely inserted in file names
    """

    now = datetime.now() if not use_utc else datetime.utcnow()

    if incl_date and incl_time:
        return now.strftime('%Y-%m-%d %H:%M:%S')
    elif incl_date:
        return now.strftime('%Y-%m-%d')
    else:
        return now.strftime('%H:%M:%S')


def str_file_safe_timestamp(incl_date=True, incl_time=True, use_utc=False):
    """ Readable date/time stamp safe to include in file names

    Contains no spaces or characters illegal in file names

    :param incl_date: whether to include the date portion of the timestamp, defaults to True
    :param incl_time: whether to include the time portion of the timestamp, defaults to True
    :param use_utc: whether to use UTC time (instead of system timezone), defaults to False
    :return: a timestamp string that can be safely inserted in file names
    """

    now = datetime.now() if not use_utc else datetime.utcnow()

    if incl_date and incl_time:
        return now.strftime('%Y-%m-%d_T%H-%M-%S')
    elif incl_date:
        return now.strftime('%Y-%m-%d')
    else:
        return now.strftime('T%H-%M-%S')
