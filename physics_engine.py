import time
import logging
from typing import List

import numpy as np
from numba import njit
import vtkplotter as vp

from point_mass import PointMass

# Coulomb constant, N*m^2/C^2
K_COULOMB = 8987551787.3681764
# Universal gravitational constant, N*m^2/kg^2
G_UNIVERSAL = 6.670408e-11


class PhysicsEngine:

    def __init__(self, dt=0.1, fps=30.0, enable_gravity=True, enable_charge=True):

        self._dt = dt
        self._fps = fps
        self._gravity_enabled = enable_gravity
        self._charge_enabled = enable_charge

        # Lists of objects in the simulation, based on their properties
        self._sim_objects: List[PointMass] = []
        self._objects_nonzero_charge: List[PointMass] = []
        self._objects_significant_charge: List[PointMass] = []
        self._objects_nonzero_gravity: List[PointMass] = []
        self._objects_significant_gravity: List[PointMass] = []

    def add_object(self, obj: PointMass):
        """ Adds an object to the physics simulation

        :param obj: the object to add
        """
        self._sim_objects.append(obj)

        if obj.charge != 0.0:
            self._objects_nonzero_charge.append(obj)

            if not obj.negligible_charge:
                self._objects_significant_charge.append(obj)

        if obj.mass != 0.0:
            self._objects_nonzero_gravity.append(obj)

            if not obj.negligible_gravity:
                self._objects_significant_gravity.append(obj)

    def __iadd__(self, obj: PointMass):
        """ Shorthand for adding an object to the physics simulation

        e.g. `phys_sim += phys_object`

        :param obj: the object to add to the simulation
        :return: the PhysicsEngine, now including the new object
        """
        self.add_object(obj)
        return self

    def simulate(self, t_max=-1.0):
        plotter = vp.Plotter(title='The World', bg2=(0, 0, 0), bg=(40, 40, 40), interactive=False, axes=4,
                             size=(600, 800))

        bounds = vp.Cube(side=10, c='white')
        bounds.wireframe()
        plotter += bounds

        # Create graphical objects for each object
        vp_objects = {}
        for obj in self._sim_objects:
            vp_obj = vp.Point(obj.pos, r=obj.size, c=obj.color)
            vp_obj.addTrail(lw=0.5)
            plotter += vp_obj
            vp_objects[obj] = vp_obj

        # Next time to render a frame, based on the FPS setting
        next_render_time = time.monotonic()

        next_print_time = time.monotonic()

        # Main loop
        t = 0.0
        while True:
            # Reset forces for next iteration
            for obj in self._sim_objects:
                obj.force = np.array((0.0, 0.0, 0.0))

            # Electrostatic force
            if self._charge_enabled:
                for a in self._objects_significant_charge:
                    for b in self._objects_nonzero_charge:
                        if a is b:
                            continue
                        f = self._calculate_electrostatic(a, b)
                        b.force += f
                        a.force -= f

            # Gravity
            if self._gravity_enabled:
                for a, b in zip(self._objects_significant_gravity, self._objects_nonzero_gravity):
                    if a is b:
                        continue
                    f = self._calculate_gravity(a, b)
                    b.force += f
                    a.force -= f

            # Calculate movement
            for obj in self._sim_objects:
                if obj.fixed:
                    continue

                accel = obj.force / obj.mass
                obj.vel += accel * self._dt
                obj.pos += obj.vel * self._dt

            # Render a frame
            if time.monotonic() >= next_render_time:
                next_render_time += 1.0 / self._fps

                for obj in self._sim_objects:
                    vp_objects[obj].pos(*obj.pos)

                plotter.show(interactive=False)

            if time.monotonic() >= next_print_time:
                next_print_time += 0.2

                print(self._sim_objects[0].vel)

            # End time
            if t_max > 0 and t >= t_max:
                print('Stop time reached. Exiting simulation.')
                break

            t += self._dt
            time.sleep(self._dt)

    @staticmethod
    def _calculate_electrostatic(a: PointMass, b: PointMass):
        """ Calculates the electrostatic force between two particles.

        Per Coulomb's law,
            F = k * q1 * q2 / r^2 in the direction of r
        And the force on the other particle is the negative of this force

        :param a: the first particle
        :param b: the second particle
        :return: the force of particle `a` on particle `b` as a 3D vector (x, y, z) in Newtons
        """
        displacement = b.pos - a.pos
        dist_squared = np.dot(displacement, displacement)
        direction = displacement / np.sqrt(dist_squared)

        return (K_COULOMB * a.charge * b.charge / dist_squared) * direction

    @staticmethod
    def _calculate_gravity(a: PointMass, b: PointMass):
        """ Calculates the gravitational force between two particles.

        Per Newton's law of universal gravitation,
            F = G * m1 * m2 / r^2 in the direction of r
        And the force on the other particle is the negative of this force

        :param a: the first particle
        :param b: the second particle
        :return: the force of particle `a` on particle `b` as a 3D vector (x, y, z) in Newtons
        """
        displacement = b.pos - a.pos
        dist_squared = np.dot(displacement, displacement)
        direction = displacement / np.sqrt(dist_squared)

        return (G_UNIVERSAL * a.mass * b.mass / dist_squared) * direction
