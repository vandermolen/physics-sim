from physics_engine import PhysicsEngine
from point_mass import PointMass

import numpy as np


def main():
    sim = PhysicsEngine(dt=0.01, enable_gravity=False)

    qe = 1.6e-19
    me = 0.9e-30

    sim += PointMass(np.array((-5, -5.0, 0.0)), vel=np.array((3.0, 0.0, 1.0)), mass=1*me, charge=1*qe, color='r')
    sim += PointMass(np.array((5, 5.0, 0.0)), vel=np.array((-3.0, 0.0, -1.0)), mass=1*me, charge=-1*qe, color='g')

    sim.simulate(t_max=5.0)


if __name__ == '__main__':
    main()
